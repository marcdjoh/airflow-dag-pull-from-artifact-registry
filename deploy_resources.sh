# Configure cloud shell to use the proper GCP project.
gcloud config set project <project_id>
# Activate the artifact registry API
gcloud services enable artifactregistry.googleapis.com
# Create a python repository
gcloud artifacts repositories create <your_repository_name> --repository-format=python --location=<your_repository_location> \
--description="<your_repository_description>"
# Clone the sample python package
git clone https://gitlab.com/marcdjoh/sample-python-package.git
# Create a python virtual environment (command for python3)
python3 -m venv py3-env
# Activate the python virtual environment
source py3-env/bin/activate
# Install twine, keyrings.google-artifactregistry-auth and wheel
pip install twine keyrings.google-artifactregistry-auth wheel
# Build the sample python package
python setup.py bdist_wheel
# Authenticate to your GCP project
gcloud config set account <your_gcp_account_email>
gcloud config set project <your_gcp_project>
gcloud auth application-default login
# Deploy the sample python package to artifact registry
twine upload https://<your_repository_location>-python.pkg.dev/<your_gcp_project>/<your_repository_name> dist/*
# Clone the airflow dag repository
git clone https://gitlab.com/marcdjoh/cloud-functions-pull-from-artifact-registry.git
# Create a service account for the composer environment
gcloud iam service-accounts create <your_service_account_id>
# Add the composer worker role to the service account
gcloud projects add-iam-policy-binding <project_id> \
  --member=serviceAccount:<your_service_account_id>@<project_id>.iam.gserviceaccount.com \
  --role=roles/composer.worker
# Deploy the cloud composer environment
gcloud composer environments create <your_environment_name> --location=<your_location_name> \
  --zone=<your_zone_name> --service-account=<your_service_account_id>@<project_id>.iam.gserviceaccount.com \
  --python-version=3 --image-version=composer-1.17.8-airflow-2.1.4